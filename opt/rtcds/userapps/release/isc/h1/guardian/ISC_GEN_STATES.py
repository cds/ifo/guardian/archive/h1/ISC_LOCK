import subprocess

from guardian import GuardState, GuardStateDecorator
import ISC_library
import cdsutils as cdu
import time
from timeout_utils import call_with_timeout
import math
import lscparams

##################################################
# Take care of WFS centering
##################################################

# need to include POP?
def gen_WFS_DC_CENTERING(dof,port):
    class WFS_DC_CENTERING(GuardState):
        request = False

        @ISC_library.assert_dof_locked_gen(['IMC',dof])
        def main(self):
            log('Port is: %s'%port)
            log('DOF is: %s'%dof)
            if port == 'REFL':
                self.servos = [1,2]
                self.sus    = ['RM1','RM2']
            elif port == 'AS':
                self.servos = [3,4]
                self.sus    = ['OM1','OM2']
            elif port == 'REFL_AS':
                self.servos = [1,2,3,4]
                self.sus    = ['RM1','RM2','OM1','OM2']
            log(self.servos)

            if 'AS' in port:
                if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
                    notify('Toast is ready!')
                    ezca['ISI-HAM6_SATCLEAR'] = 1  #reset saturation counter
                    ezca['ISI-HAM6_WD_RSET'] = 1
                    time.sleep(.1)
                    ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1 # open fast shutter

            log('turning on DC centering')
            for py in ['P','Y']:
                for servo in self.servos:
                    ezca.get_LIGOFilter('ASC-DC%s_%s'%(servo,py)).switch_on('INPUT')

        @ISC_library.assert_dof_locked_gen(['IMC',dof])
        def run(self):
            if not ISC_library.WFS_DC_centering_servos_OK(port):
                notify('WFS DC centering railed')
                if 'AS' in port:
                    if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
                        notify('Opening fast shutter')
                        ezca['ISI-HAM6_SATCLEAR'] = 1  #reset saturation counter
                        ezca['ISI-HAM6_WD_RSET'] = 1
                        time.sleep(.1)
                        ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1 # open fast shutter
                for py in ['P','Y']:
                    for servo in self.servos:
                        ezca.get_LIGOFilter('ASC-DC%s_%s'%(servo,py)).switch_off('INPUT')
                        ezca['ASC-DC%s_%s_RSET'%(servo,py)] = 2
                    for sus in self.sus:
                        ezca['SUS-%s_M1_LOCK_%s_RSET'%(sus,py)] = 2
                time.sleep(3)
                for py in ['P','Y']:
                    for servo in self.servos:
                        ezca.get_LIGOFilter('ASC-DC%s_%s'%(servo,py)).switch_on('INPUT')
                
            servolist = []
            for py in ['P','Y']:
                for servo in self.servos:
                    servolist.append('ASC-DC%s_%s_INMON'%(servo,py))
            inmonvals = cdu.avg(2,servolist)
            if any(x >= 0.2 for x in inmonvals):
                return False
            else:
                return True

    return WFS_DC_CENTERING


##################################################
# Offload alignment to sliders
##################################################


def gen_OFFLOAD_ALIGNMENT_MANY(dof,ramptime,opticList):
    class OFFLOAD_ALIGNMENT(GuardState):
        request = False
        redirect = False

        #@ISC_library.assert_dof_locked_gen(['IMC',dof])
        # SED note July 22nd 2018.  This was causing the green WFS to not get offloaded because it would fail this check even when the arm stayed locked.  Since this is a redirect = fasle state, I'm not sure if we want this decorator (on this state, because if we loose lock in the midlle of offlaoding, we still want to finish the offloading not stop partway through.
        def main(self):
            log('starting smooth offload') # formerly separated out as smooth_offload_fast function
            log(opticList)

            # set up names for each optic and DOF to offload
            numOffload = 2 * len(opticList)
            n_dof = []
            n_optic = []
            n_top_stage = []

            for index in range(len(opticList)):
                optic = opticList[index]       # this optic

                # the top stage for this optic
                if (optic[:3] == ('ETM')) or (optic[:3] == ('ITM')):
                    top_stage = 'M0'
                else:
                    top_stage = 'M1'

                # the optic, dof, and top stage for each index
                n_dof += ['P', 'Y']
                n_optic += [optic, optic]
                n_top_stage += [top_stage, top_stage]

            # set up LIGOFilters
            lock_fm = []
            drivealign_fm = []
            opticalign_fm = []

            for index in range(numOffload):
                dof = n_dof[index]
                optic = n_optic[index]
                top_stage = n_top_stage[index]

                lock_fm.append(ezca.get_LIGOFilter('SUS-{0}_{1}_LOCK_{2}'.format(optic, top_stage, dof)))
                drivealign_fm.append(ezca.get_LIGOFilter('SUS-{0}_{1}_DRIVEALIGN_{2}2{2}'.format(optic, top_stage, dof)))
                opticalign_fm.append(ezca.get_LIGOFilter('SUS-{0}_{1}_OPTICALIGN_{2}'.format(optic, top_stage, dof)))

            # record initial state
            lock_gain = []
            lock_tramp = []
            opticalign_tramp = []

            for index in range(numOffload):
                lock_gain.append(lock_fm[index].GAIN.get())
                lock_tramp.append(lock_fm[index].TRAMP.get())
                opticalign_tramp.append(opticalign_fm[index].TRAMP.get())

            # simultaneously ramp LOCK gain to zero, and
            for index in range(numOffload):
                initial_bias = opticalign_fm[index].OFFSET.get()
                added_bias = drivealign_fm[index].OUTPUT.get()/opticalign_fm[index].GAIN.get()
                desired_bias = round(initial_bias + added_bias, 4)
                
                lock_fm[index].switch_off('INPUT')
                lock_fm[index].ramp_gain(0, ramp_time=ramptime, wait=False)
                opticalign_fm[index].ramp_offset(desired_bias, ramp_time=ramptime, wait=False)

            # wait for all ramps to complete
            log('waiting for ramps to finish...')
            time.sleep(ramptime)
            log('done waiting')

            # restore initial state
            for index in range(numOffload):
                ezca[lock_fm[index].filter_name + '_RSET'] = 2
                time.sleep(1) # To prevent sus from getting blasted by input turning on before counts are cleared
                lock_fm[index].switch_on('INPUT')
                lock_fm[index].ramp_gain(lock_gain[index], ramp_time=lock_tramp[index], wait=False)
                ezca[opticalign_fm[index].filter_name + '_TRAMP'] = opticalign_tramp[index]

        #@ISC_library.assert_dof_locked_gen(['IMC',dof])
        def run(self):
            return True

    return OFFLOAD_ALIGNMENT




##################################################
# ALS fine tune IR (used for COMM and DIFF)
##################################################

def gen_TUNE_IR_BETTER(SweepWidth, StepSize, pause, Thresh, TransChan, SweepChan, SleepTime, dof, ThreshLow=0.1):
    #dof should be 1 for comm, 2 for diff
    class TUNE_IR_BETTER(GuardState):
        request = False
        index = 24

        @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN', 'YARM_GREEN'])
        def main(self):
            self.SweepWidth = SweepWidth  # width of the sweep is +/- this number
            self.StepSize = StepSize
            self.SleepTime = SleepTime
            self.pause = pause # time to pause for each step (seconds)
            self.threshHigh = Thresh   # threshold on transmitted power to jump to next state
            self.threshLow = ThreshLow
            self.NumSteps = 2*math.floor(self.SweepWidth/self.StepSize) # total number of steps
            self.counter = 0
            self.StartPoint = ezca[SweepChan]
            self.MaxTrans = ezca[TransChan]
            self.transValueOld = ezca[TransChan]
            self.transValueNew = ezca[TransChan]
            self.transValueMax = ezca[TransChan]
            self.direction = 1 # positive or negative
            self.MaxTransOffset = ezca[SweepChan]
            self.timer['pause'] = self.pause

        @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN', 'YARM_GREEN'])
        def run(self):
            # check green arm build ups before finding IR
            if ezca['ALS-C_TRX_A_LF_OUTMON'] < 0.65:    # Should be 0.85 12Aug2020 JCD ## Was 0.75 12Dec 2023 TJ
                notify('X arm alignment is bad, not searching')
                time.sleep(1)
            # also check Y arm if searching for diff offset
            elif dof == 2 and ezca['ALS-C_TRY_A_LF_OUTMON'] < 0.75:  # Should be 0.85 12Aug2020 JCD
                notify('Y arm alignment is bad, not searching')
                time.sleep(1)
            else:
                # Take the max value during this time to help with times of high ground motion
                self.transValueNew = call_with_timeout(cdu.getdata, TransChan, 2).data.max()
                #self.transValueNew = call_with_timeout(cdu.avg, 2, TransChan)
                # If no data 
                if not self.transValueNew:
                    return False
                log('Transmitted value is {}'.format(self.transValueNew))
                if self.transValueNew < self.threshLow:
                    log('Transmitted power too low for FINE_TUNE_IR.')
                    return 'NO_IR_FOUND'
                elif self.transValueNew >= self.threshHigh:
                    log('Transmitted power sufficient.')
                    return True
                elif self.counter < self.NumSteps:
                    if self.transValueNew < self.transValueOld:
                        self.direction *= -1
                    ezca[SweepChan] += self.StepSize * self.direction
                    self.transValueOld = self.transValueNew
                    time.sleep(self.SleepTime)
                else:
                    log('Fine tuning failed.')
                    return 'NO_IR_FOUND'
    return TUNE_IR_BETTER

def gen_NOMINAL_LOW_NOISE(nodes, switch_SDF):
    class NOMINAL_LOW_NOISE(GuardState):
        request = True

        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @ISC_library.assert_dof_locked_gen(['Full_IFO'])
        @nodes.checker()
        @ISC_library.unstall_nodes(nodes)
        def main(self):
            # Switch all SDF source files to OBSERVE
            switch_SDF.set_source_files('OBSERVE')
            #clear history of blrms
            for blrms in range(1,11):
                ezca['OAF-RANGE_RBP_%s_GAIN'%blrms] = 1000
                ezca['OAF-RANGE_RLP_{}_RSET'.format(blrms)]=2

            for ADS in ['3', '4', '5']:
                for dof in ['PIT', 'YAW']:
                    ezca['ASC-ADS_%s%s_DOF_LIMIT'%(dof, ADS)] = 100
                    ezca.get_LIGOFilter('ASC-ADS_%s%s_DOF'%(dof, ADS)).switch_on('LIMIT')
                    
            #nodes['AWG_LINES'] = 'INJECTING'  # Use this if checking TCS-type things during Observe.  Should nominally be commented out
            
            #nodes['TMS_SERVO'] = 'TMS_SERVO_ON'

            self.counter = 0

        @ISC_library.get_subordinate_watchdog_check_decorator(nodes)
        @ISC_library.assert_dof_locked_gen(['Full_IFO'])
        @nodes.checker()
        @ISC_library.unstall_nodes(nodes)
        def run(self):
            # adjust RPC as the IFO thermalizes
            #ISC_library.adjust_radiation_pressure_compensation()
            #why are we doing this check here?
            if nodes['OMC_LOCK'] == 'READY_FOR_HANDOFF':
                return True
            else:
                notify('waiting for third stage of OMC whitening to be engaged')
	
                
    return NOMINAL_LOW_NOISE




