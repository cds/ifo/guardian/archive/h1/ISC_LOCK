"""
Create a interface that can be used in Guardian and also from the
command line that will switch a the SDF source files to and from
Observe.

All of the names are assumed h1. For now.

Double check the difference between the down and observe scripts.

Check timing in Guardian if the note is realor not.
Speed things up?

"""
import time
import cdslib

exclude_dict = {}

# Make dict with all models
femodels = list(cdslib.get_all_models())
all_fe_dict = {m.DCUID: m.name for m in femodels}
# Remove the excluded
fe_dict = {key: val for key, val in all_fe_dict.items() \
           if key not in exclude_dict}


# Make a SEI dictionary
isi_fe_dict = {dcuid: model for dcuid, model in fe_dict.items() if 'isi' in model}
hpi_fe_dict = {dcuid: model for dcuid, model in fe_dict.items() if 'hpi' in model}

sei_fe_dict = isi_fe_dict.copy()
sei_fe_dict.update(hpi_fe_dict)

# Make an ecat dictionary
ecat_fe_dict = {dcuid: model for dcuid, model in fe_dict.items() if 'ecat' in model}




def set_source_files(source_file):
    """The main function that will do the switching.

    source_file  - A string of either 'OBSERVE' or 'safe'.
    """

    if source_file == 'safe':
        # Keep the sei in OBSERVE always
        ex_dict = exclude_dict.copy()
        ex_dict.update(sei_fe_dict)
        models_dict = {dcuid: model for dcuid, model in fe_dict.items() if dcuid not in ex_dict}
    else:
        models_dict = fe_dict.copy()
    # Verify MASK is ON so unmonitored channels do not create diffs in observing
    if source_file == 'OBSERVE':
        for fe in models_dict:
            ezca['FEC-{}_SDF_MON_ALL'.format(fe)] = 0 # 0 = MASK, 1 = ALL
            
    # Do this in two loops so it can get done faster
    for fe in models_dict:
        ezca['FEC-{}_SDF_NAME'.format(fe)] = source_file
    # Does the sleep need to be this long if we loop it like this?
    log('One Mississippi')
    time.sleep(1)
    for fe in models_dict:
        ezca['FEC-{}_SDF_RELOAD'.format(fe)] = 2
